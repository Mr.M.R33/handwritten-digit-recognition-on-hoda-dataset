import keras as kr
from hoda_dataset_helper import read_hoda
from keras.utils import to_categorical

X_train, Y_train, X_test, Y_test, X_remaining, Y_remaining = read_hoda()

#============= **************Evaluate For Data test HODA************* =========
model = kr.models.load_model('Traindata_HODA.h5')
loss , acc = model.evaluate(X_test , Y_test , batch_size=128 , verbose=True)
print(loss,acc)


