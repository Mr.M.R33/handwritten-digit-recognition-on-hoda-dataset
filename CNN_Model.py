import keras as kr
from hoda_dataset_helper import read_hoda
from keras.models import Sequential
from keras.utils import to_categorical
from keras.layers import Dense, Conv2D, MaxPool2D, Flatten, Dropout

NUMBER_OF_CLASS = 10

X_train, Y_train, X_test, Y_test, X_remaining, Y_remaining = read_hoda()

# ====================****define convolutional model*****======================

def my_cnn_model(size):
    model = Sequential()

    model.add(Conv2D(64, (3, 3), padding='same', input_shape=size, activation='relu'))
    model.add(MaxPool2D(pool_size=(2, 2)))
    model.add(Dropout(0.2))

    model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
    model.add(MaxPool2D(pool_size=(2, 2)))
    model.add(Dropout(0.2))

    model.add(Conv2D(128, (3, 3), padding='same', activation='relu'))
    model.add(MaxPool2D(pool_size=(2, 2)))
    model.add(Dropout(0.2))

    model.add(Conv2D(150, (3, 3), padding='same', activation='relu'))
    model.add(MaxPool2D(pool_size=(2, 2)))
    model.add(Dropout(0.2))

    model.add(Conv2D(180, (3, 3), padding='same', activation='relu'))
    model.add(MaxPool2D(pool_size=(2, 2)))
    model.add(Dropout(0.2))

    model.add(Flatten())

    return model


# ==== *********************train on HODA  dataset************ =======
model = my_cnn_model(size=(32, 32, 1))

model.add(Dense(128, activation='relu', name='first_dense'))
model.add(Dense(NUMBER_OF_CLASS, activation='softmax', name='out'))

optimaze = kr.optimizers.Adam(lr=0.001)
model.compile(optimizer=optimaze, loss='categorical_crossentropy', metrics=['accuracy'])
model.fit(X_train, Y_train, batch_size=128, epochs=8, verbose=True)
model.save_weights("hodaWeights.h5")

model.save('Traindata_HODA.h5')
# ======================================================================
